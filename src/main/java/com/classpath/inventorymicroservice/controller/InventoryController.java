package com.classpath.inventorymicroservice.controller;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/inventory")
public class InventoryController {

    private int counter = 100;

    @PostMapping
    public int updateQty(){
        return --counter;
    }
}
